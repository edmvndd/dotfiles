#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#export PS1='\e[0;37m[\e[0;35m\u\e[0;37m@\e[0;32m\h\e[0;37m] \e[0;37m\w '
#export PS1='\e[0;37m[\u@\h \w] '
#export PS1='\e[0;37m(\u@\h) \w '
export MOZ_ENABLE_WAYLAND=1 firefox
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_DATA_DIRS="/usr/share:/usr/local/share"
export PATH="$HOME/.local/bin:$PATH"
export EDITOR='vim'
#export TERM='st'
export BROWSER='firefox'
export PF_INFO="ascii title os kernel wm uptime pkgs memory"
export MANPAGER='nvim +Man!'

# vi mode
set -o vi
bind -m vi-insert "\C-h.":backward-kill-word

# ALIASES
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias exa='eza'
alias ls='exa --sort=extension --group-directories-first --icons'
alias ll='exa -l --sort=extension --group-directories-first --time=modified --icons'
alias lla='exa -l -a --sort=extension --group-directories-first --time=modified --icons'
alias la='exa -a --sort=extension --group-directories-first --icons'
alias l='exa -1 --sort=extension --group-directories-first --icons'
alias v='vim'
alias vc='vim config.h'
alias vx='vim ~/.xinitrc'
alias vb='vim ~/.bashrc'
alias vv='vim ~/.vimrc'
alias vs='vim ~/.config/sway/config'
alias vf='vim ~/.config/foot/foot.ini'
alias nv='nvim'
alias sx='vim ~/.config/sxhkd/sxhkdrc'
alias sb='source ~/.bashrc && echo Sourced ~/.bashrc'
alias ga='git add'
alias gc='git commit'
alias gp='git push'
alias gs='git status'
alias c='clear'
alias ns='nsxiv'
alias sudo='doas'
alias paclog='cat /var/log/pacman.log'
alias playdoom='cd /usr/share/games/brutal-doom && gzdoom -iwad ~/Downloads/DOOM.WAD -file brutal-doom.pk3'
alias playdoom2='cd /usr/share/games/brutal-doom && gzdoom -iwad /usr/share/games/brutal-doom/DOOM2.WAD -file brutal-doom.pk3'
alias netreset='sudo systemctl restart NetworkManager && echo network restarted!'
alias news='newsboat'
alias wget='wget -q'
alias docs='cd ~/Documents'
alias hst='history | tail -n 5'
alias checkup='checkupdates | wc -l'
alias checkudpates='checkupdates'
alias keepassxcls='keepassxc-cli show -s /home/ed/Passwords.kdbx'
alias z='zathura'
alias updatemirrors='reflector --download-timeout 10 --country US --latest 20 --fastest 20 --sort score'
alias ff='fastfetch'
#alias z='swayhide zathura'

# fancy prompt
#eval "$(starship init bash)"

# simple prompt
. ~/.local/bin/promptless.sh
