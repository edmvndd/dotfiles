#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

[ "$(tty)" = "/dev/tty2" ] && exec sway
[ "$(tty)" = "/dev/tty1" ] && exec startx
