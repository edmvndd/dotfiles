# Created by newuser for 5.9
#
# ~/.zshrc

export PF_INFO="ascii title os kernel wm uptime pkgs memory"
export MOZ_ENABLE_WAYLAND=1 firefox
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
export PATH="$HOME/scripts:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export EDITOR='vim'
export BROWSER='firefox'
export SHELL='zsh'

# ALIASES
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias ls='exa --sort=extension --group-directories-first --icons'
alias ll='exa -l --sort=extension --group-directories-first --time=modified --icons'
alias lla='exa -l -a --sort=extension --group-directories-first --time=modified --icons'
alias la='exa -a --sort=extension --group-directories-first --icons'
alias l='exa -1 --sort=extension --group-directories-first --icons'
alias v='vim'
alias vc='vim config.h'
alias vx='vim ~/.xinitrc'
alias vb='vim ~/.bashrc'
alias vv='vim ~/.vimrc'
alias vs='vim ~/.config/sway/config'
alias vf='vim ~/.config/foot/foot.ini'
alias sx='vim ~/.config/sxhkd/sxhkdrc'
alias sb='source ~/.bashrc'
alias ga='git add'
alias gc='git commit'
alias gp='git push'
alias gs='git status'
alias c='clear'
alias ns='nsxiv'
alias sudo='doas'
alias paclog='cat /var/log/pacman.log'
alias z='zathura'
alias news='newsboat'
alias wget='wget -q'
alias docs='cd ~/Documents'
alias hst='history | tail -n 5'
alias checkup='checkupdates'
alias amireal='echo yes you are'

# fancy prompt
#eval "$(starship init zsh)"

# simple prompt
#. ~/.local/bin/promptless.sh
export PROMPT='(%/%)  --> '
