set background=dark
syntax enable

set tabstop=4
set shiftwidth=4
set relativenumber
set nu
set noswapfile
set nobackup
set incsearch
set splitbelow
set clipboard=unnamed

let mapleader = " "
let &t_SI = "\e[6 q"
"let &t_SR = "\e[4 q"
let &t_EI = "\e[2 q"

" airline configuration
let g:airline_powerline_fonts = 1

call plug#begin('~/.vim/plugged')

Plug 'morhetz/gruvbox'
Plug 'preservim/nerdtree'
Plug 'bluz71/vim-moonfly-colors'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'dracula/vim'
Plug 'tribela/vim-transparent'
Plug 'altercation/vim-colors-solarized'
Plug 'dylanaraps/fff.vim'

call plug#end()

"let g:solarized_termcolors=256
colorscheme gruvbox

" write the file
nnoremap <leader>w :w<CR>
" write and quit
nnoremap <leader>e :wq<CR>
" quit without saving
nnoremap <leader>q :q!<CR>
" save/write and source this file
nnoremap <leader>s :w <bar> :source %<CR> 

inoremap <C-H> <C-W>

map <Leader>tk <C-w>t<C-w>H
map <Leader>th <C-w>t<C-w>K

" split navigation
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>l :wincmd l<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>

" open file explorer
nnoremap <leader>n :NERDTree<CR>
"nnoremap <leader>n :wincmd v<bar> :Ex <bar> :vertical resize 25 <bar> :wincmd r<CR>

" resizing splits
nnoremap <silent> <C-Up> :resize +5<CR>
nnoremap <silent> <C-Down> :resize -5<CR>
nnoremap <silent> <C-Left> :vertical resize +5<CR>
nnoremap <silent> <C-Right> :vertical resize -5<CR>

" IDK WHAT THESE DO
"nnoremap <leader>d "_d
"vnoremap <leader>d "_d
"vnoremap <leader>p "_dP
